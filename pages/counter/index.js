import React from "react";
import Link from "next/link";
import { Button } from 'semantic-ui-react'
import Counter from "../../components/Counter";
require('./counter.page.less');

function CounterPage() {
  return (
    <div className="c-counter">
      <Button>Hello button</Button>
      <Counter />
      <Link href="/">
        <a>About Page</a>
      </Link>
    </div>
  );
}

export default CounterPage;
