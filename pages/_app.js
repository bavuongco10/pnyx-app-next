import React from "react";
import 'semantic-ui-css/semantic.min.css'

import './styles.css'
import Layout from "../components/Layout";

const MyApp = ({ Component, pageProps }) => (
  <Layout>
    <Component {...pageProps} />
  </Layout>
)

export default MyApp



