export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const RESET = "RESET";

export function increment() {
  return function(dispatch) {
    dispatch({ type: INCREMENT });
  };
}

export function decrement() {
  return function(dispatch) {
    dispatch({ type: DECREMENT });
  };
}

export function reset() {
  return function (dispatch) {
    dispatch({type: RESET});
  };
}

const initialState = { counter: 0 };

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case INCREMENT:
      return { counter: state.counter + 1 };
    case DECREMENT:
      return { counter: state.counter - 1 };
    case RESET:
      return { counter: 0 };
    default:
      return state;
  }
};
