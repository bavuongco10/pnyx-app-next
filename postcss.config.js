module.exports = {
  "plugins": process.env.NODE_ENV === 'production' ? [
    "postcss-flexbugs-fixes",
    [
      "postcss-preset-env", {
        "autoprefixer": {
          "flexbox": "no-2009"
        },
        "stage": 3,
        "features": {
          "custom-properties": false
        }
      },
      [
        "cssnano", {
          preset: [
            'default',
            {
              discardComments: {
                removeAll: true,
              },
            },
          ],
        }
      ]
    ],
    [
      '@fullhuman/postcss-purgecss', {
        content: [
          './pages/**/*.{js,jsx,ts,tsx}',
          './components/**/*.{js,jsx,ts,tsx}',
          './node_modules/semantic-ui-react/**/*.{js,jsx}',
        ],
        defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
        safelist: ["html", "body"]
      }
    ],
  ]: []
}
