const withPlugins = require("next-compose-plugins");
const withAntdLess = require('./next-plugin-antd-less');

const plugins = [
  [withAntdLess({
    cssLoaderOptions: {},
    webpack(config) {
      return config;
    },
  })]
]

module.exports = withPlugins(plugins)




